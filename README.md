## Installation

```
git clone git clone https://bchurlinov@bitbucket.org/bchurlinov/bojan-test-project.git
npm i
```

## Usage

### Development server

```bash
npm start
```

### Production build

```bash
npm run build
```
