const template = `
<div class="tabs-container">
<div id="about" class="tabcontent">
<div class="multiple-inputs animated">
    <div class="multiple-inputs__header">
        <div>
            <h3>About</h3>
        </div>
        <div>
            <button data-close-multiple="close" class="button-default button-small">Cancel</button>
            <button data-close-multiple="save" data-save-multiple="save" class="button-default button-small">Save</button>
        </div>
    </div>
    <div class="multiple-inputs__container"></div>
</div>
<div class="animated about-content">
    <div class="about-content__wrapper">
        <div>
            <h3>About</h3>
        </div>
        <div>
            <button data-toggle="multiple" class="button-cta">
                <ion-icon name="pencil-outline"></ion-icon>
            </button>
        </div>
    </div>
    <div class="about-content__editable">
        <div data-user="name" class="editable_wrap">
            <ion-icon name="person-outline"></ion-icon> <p>Jessica Parker</p> <span><ion-icon name="pencil-outline"></ion-icon></span>
        </div>
        <div data-user="location" class="editable_wrap">
            <ion-icon name="location-outline"></ion-icon> <p>Newport Beach, CA</p> <span><ion-icon name="pencil-outline"></ion-icon></span>
        </div>
        <div data-user="phone" class="editable_wrap">
            <ion-icon name="call-outline"></ion-icon> <p>(949) 325 - 68594</p> <span><ion-icon name="pencil-outline"></ion-icon></span>
        </div>
        <div data-user="website" class="editable_wrap">
            <ion-icon name="globe-outline"></ion-icon> <p>www.seller.com</p> <span><ion-icon name="pencil-outline"></ion-icon></span>
        </div>
    </div>
</div>
</div>

<div id="settings" class="tabcontent">
    <div class="fadeIn animated">
        <h3>Settings</h3>
    </div>
</div>

<div id="options" class="tabcontent">
    <div class="fadeIn animated">
        <h3>Options</h3>
    </div>
</div>

</div>
`


const Content = () => {
    return template
}

export default Content;