// Attach functions to tooltip buttons
export const tooltipButtonsAttachListeners = () => {

    const changeButton = document.querySelectorAll("[data-button-change]");
    changeButton.forEach(button => {
        const buttonAttr = button.getAttribute("data-button-change");
        button.addEventListener("click", function () {
            changeInputData(buttonAttr)
        });
    });

    const closeButton = document.querySelectorAll("[data-button-close]");
    closeButton.forEach(button => {
        const buttonAttr = button.getAttribute("data-button-close");
        button.addEventListener("click", function () {
            closeTooltip(buttonAttr)
        });
    });

    const toggleMultiple = document.querySelector("[data-toggle]");
    toggleMultiple.addEventListener("click", function () {
        showMultipleInputsContainer();
    });

    const closeMultiple = document.querySelector("[data-close-multiple]");
    closeMultiple.addEventListener("click", function () {
        closeMultipleInputsContainer();
    });
}

// Add functions to the tooltips
export const addShowTooltips = () => {
    const spans = document.querySelectorAll("[data-user] span");
    spans.forEach((item, index) => {
        item.addEventListener("click", function () {
            showTooltip(popovers[index])
        });
    });

    tooltipButtonsAttachListeners();
}

// Toggle Tooltips
const showTooltip = (popover) => {

    const findPopover = document.querySelector(`[data-id='${popover}']`);
    const dataUser = document.querySelector(`[data-user='${popover}']`);

    const spans = document.querySelector(`[data-user='${popover}'] span ion-icon`);
    spans.getAttribute("name") === "pencil-outline" ? spans.setAttribute("name", "close-outline") : spans.setAttribute("name", "pencil-outline");


    const elements = document.querySelectorAll(".popover");
    const bounds = dataUser.getBoundingClientRect();

    elements.forEach(item => {
        item.style.left = `${bounds.width + 15}px`
    })

    findPopover.classList.toggle("popover-active");
}

// On Button "Cancel" close popover
const closeTooltip = (popover) => {
    const findPopover = document.querySelector(`[data-id="${popover}"]`);
    findPopover.classList.remove("popover-active");

    const spans = document.querySelector(`[data-user='${popover}'] span ion-icon`);
    spans.getAttribute("name") === "pencil-outline" ? spans.setAttribute("name", "close-outline") : spans.setAttribute("name", "pencil-outline");
}

// Add popovers dynamically
const popovers = [];
export const addPopovers = () => {

    const editables = document.querySelectorAll("[data-user]");
    editables.forEach(item => {
        popovers.push(item.getAttribute(["data-user"]));
    });

    for (let i = 0; i < popovers.length; i++) {
        const elements = document.querySelector(`[data-user='${popovers[i]}']`)
        const getContents = document.querySelector(`[data-user='${popovers[i]}']`).textContent;
        const newElement = document.createElement("div");
        newElement.setAttribute("data-id", popovers[i]);
        newElement.setAttribute("class", "popover");
        newElement.innerHTML = renderInnerTemplate(popovers[i], getContents);
        newElement.style.display = "none"
        elements.parentNode.insertBefore(newElement, elements);
    }
}

// Get data from input and change it
function changeInputData(attr) {
    const inputValue = document.querySelector(`[data-single="${attr}"]`).value;
    const dataUser = document.querySelector(`[data-user="${attr}"] p`);
    const headerUser = document.querySelector(`[data-header="${attr}"]`);

    if (!inputValue) {
        alert(`${attr.toUpperCase()} input needs to contain a value`);
        return;
    }

    dataUser.textContent = inputValue;
    headerUser.textContent = inputValue;
}

// Render Inner Template function
const renderInnerTemplate = (item, contents) => {
    return `
    <div>
        <h5>${item}</h5>
        <div class="stylish-input">
            <div class="group">
            <input type="text" name="${item}" data-single="${item}" required>
            <span class="bar"></span>
            <label>${contents}</label>
            </div>
        </div>
        <div class="popover-buttons">
            <button data-button-change="${item}" class="button-default button-small">Save</button>
            <button data-button-close="${item}" class="button-default button-small">Cancel</button>
        </div>
    </div>`
}

// Input Template
export const multipleInputsTemplate = (name) => {
    return `
    <div class="stylish-input multiple-input-box">
        <div class="group">
            <input type="text" name="${name}" data-multiple="${name}" required>
            <span class="bar"></span>
            <label>${name}</label>
        </div>
    </div>`
};


// Render Input Template inside Multiple Inputs Container
export const renderMultipleInputs = () => {
    multipleInputsTemplate()

    const dataNames = [];
    const textAttr = [];

    const inputsContainer = document.querySelector(".multiple-inputs__container");
    const dataUser = document.querySelectorAll("[data-user]");

    dataUser.forEach(item => {
        dataNames.push(item.getAttribute("data-user"));
    });

    dataNames.forEach((name, index) => {
        inputsContainer.innerHTML += multipleInputsTemplate(name);
    });

    attachEventsMultipleInputs(dataNames);
};


// Attach Event Listeners to Multiple Inputs
const attachEventsMultipleInputs = (data) => {
    const dataSave = document.querySelector("[data-save-multiple]");
    dataSave.addEventListener("click", function () {
        saveMultipleInputValues(data);
    })
};

// Get and save values from the multiple inputs
const saveMultipleInputValues = (data) => {
    const dataMultiple = document.querySelectorAll("[data-multiple]");
    let count = 1;

    dataMultiple.forEach(item => {
        const attr = item.getAttribute("name");
        const headerItems = document.querySelector(`[data-header="${attr}"]`);
        const contentItems = document.querySelector(`[data-user="${attr}"] p`);

        if (item.value === "") {
            return
        } else {
            count++;
            headerItems.textContent = item.value;
            contentItems.textContent = item.value;
        }
    });

    if (count !== 1) {
        setTimeout(() => {
            alert("Changes saved successfully !");
        }, 350)
        count = 1
    };
}

const showMultipleInputsContainer = () => {
    const multipleInputsContainer = document.querySelector(".multiple-inputs");
    multipleInputsContainer.style.display = "block";
    multipleInputsContainer.classList.remove("fadeOut")
    multipleInputsContainer.classList.add("fadeIn");
}

const closeMultipleInputsContainer = () => {
    const multipleInputsContainer = document.querySelector(".multiple-inputs");
    multipleInputsContainer.classList.remove("fadeIn");
    multipleInputsContainer.classList.add("fadeOut");
    setTimeout(() => {
        multipleInputsContainer.style.display = "none";
    }, 1000)
}

