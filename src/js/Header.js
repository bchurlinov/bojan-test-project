const template = (`
<div class="header-wrap">
  <div class="header-wrap__actionButtons">
    <div>
      <div class="file-upload">
        <label for="upload" class="file-upload__label"><ion-icon name="camera-outline"></ion-icon><span>Upload cover image</span></label>
        <input id="upload" class="file-upload__input" type="file" name="file-upload">
      </div>
    </div>
    <div>
      <button class="button-default button-medium">Log Out</button>
    </div>
  </div>
  <div class="header-wrap__userinformation">
    <div>
      <div class="user-image"></div>
    </div>
    <div>
      <h3><span data-header="name">Jessica Parker<span></h3>
      <p><ion-icon name="location-outline"></ion-icon><span data-header="location">Newport Beach, CA</span></p>
      <p><ion-icon name="call-outline"></ion-icon><span data-header="phone">(949) 325 - 68594</span><p>
      <p><ion-icon name="globe-outline"></ion-icon><span data-header="website">www.seller.com</span><p>
    </div>
    <div>
      <p class="rating"></p>
      <p><b>6</b> Reviews</p>
      <div class="followers">
        <a><ion-icon name="add-circle"></ion-icon><b>15</b> Followers</a>
      </div>
    </div>
  </div>

  <div class="header-wrap__tabs tab">
  <div>
    <ul class="renderTabs"></ul>  
  </div>
  </div>
</div>
`
)

export default function Header() {
  return template
}
