import Header from "./Header";
import Content from "./Content";

const Wrapper = () => {

    // Header Component
    const header = document.createElement("div");
    header.innerHTML = Header();

    // Content Component
    const content = document.createElement("div");
    content.innerHTML = Content();

    // Wrapper Component / Export
    const wrapper = document.getElementById("wrapper");
    wrapper.appendChild(header).appendChild(content);

    return wrapper;
}

export default Wrapper;