import Wrapper from "./js/Wrapper";
import { addShowTooltips, addPopovers, renderMultipleInputs } from "./js/content_functions";
import './scss/main.scss'

window.onload = function () {
    const content = document.createElement("div");
    content.setAttribute("id", "wrapper");
    content.setAttribute("class", "container");
    document.body.appendChild(content);

    // Initialize / Render wrapper
    Wrapper();

    // Initialize addRating() to edner ratings
    addRating();

    // Initialize addTabs() to render tabs
    addTabs();

    // Initialize Popover function to add popovers dynamically
    addPopovers();

    // Add functions to the tooltips
    addShowTooltips();

    // Render Multiple Inputs
    renderMultipleInputs();

    const app = document.querySelector('#root')
    app.append(content)
}

// Add ratings dynamically
function addRating() {
    const rating = document.querySelector(".rating");
    const amount = 5;

    for (let i = 0; i < amount; i++) {

        if (i === amount - 1) {
            rating.innerHTML += "<ion-icon name='star-outline'></ion-icon>"
        } else {
            rating.innerHTML += "<ion-icon name='star'></ion-icon>"
        }
    }
}

// Add Tabs Dynamically
function addTabs() {
    const renderTabs = document.querySelector(".renderTabs");
    const tabs = ["about", "settings", "options"];
    tabs.map(item => {
        renderTabs.innerHTML += `<li class="tablinks" data-item="${item}">${item}</li>`;
    });

    const tabLinks = document.querySelectorAll(".tablinks");
    tabLinks.forEach(link => {
        const dataAttr = link.getAttribute("data-item");
        link.addEventListener("click", function () {
            tabChange(event, dataAttr)
        });
    });

    const firstTab = document.querySelector(".renderTabs > .tablinks");
    firstTab.classList.add("tab-active");
}

// Change / Swap Tabs
function tabChange(evt, dataAttr) {
    var i, tabcontent, tablinks;

    tabcontent = document.getElementsByClassName("tabcontent");

    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    tablinks = document.getElementsByClassName("tablinks");

    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" tab-active", "");
    }

    document.getElementById(dataAttr).style.display = "block";
    evt.currentTarget.className += " tab-active";
}
